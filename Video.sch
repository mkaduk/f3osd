EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:f3osd
LIBS:F3OSD-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ADG1409 U3
U 1 1 58BBDCDC
P 4300 2400
F 0 "U3" H 4300 2400 60  0000 C CNN
F 1 "ADG1409" H 4300 2300 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-16_4.4x5mm_Pitch0.65mm" H 4300 2400 60  0001 C CNN
F 3 "" H 4300 2400 60  0001 C CNN
	1    4300 2400
	1    0    0    -1  
$EndComp
Text HLabel 3350 2100 0    60   Input ~ 0
VSS
NoConn ~ 5000 2300
NoConn ~ 5000 2400
NoConn ~ 5000 2500
NoConn ~ 3550 2500
NoConn ~ 3550 2400
NoConn ~ 3550 2300
Text HLabel 5200 2100 2    60   Input ~ 0
VDD
$Comp
L GND #PWR013
U 1 1 58BC313A
P 5250 2000
F 0 "#PWR013" H 5250 1750 50  0001 C CNN
F 1 "GND" H 5250 1850 50  0000 C CNN
F 2 "" H 5250 2000 50  0000 C CNN
F 3 "" H 5250 2000 50  0000 C CNN
	1    5250 2000
	-1   0    0    1   
$EndComp
Text HLabel 5400 3100 2    60   Input ~ 0
VOUT
$Comp
L R R3
U 1 1 58BC3438
P 3400 2850
F 0 "R3" V 3480 2850 50  0000 C CNN
F 1 "10K" V 3400 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3330 2850 50  0001 C CNN
F 3 "" H 3400 2850 50  0000 C CNN
	1    3400 2850
	1    0    0    -1  
$EndComp
Text HLabel 5200 1650 2    60   Input ~ 0
SPI3
Text HLabel 3350 1650 0    60   Input ~ 0
SPI2
Text Label 5250 2300 0    60   ~ 0
VDD
Text Label 3350 2000 2    60   ~ 0
VDD
$Comp
L R R4
U 1 1 58BC389E
P 5200 2850
F 0 "R4" V 5280 2850 50  0000 C CNN
F 1 "10K" V 5200 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5130 2850 50  0001 C CNN
F 3 "" H 5200 2850 50  0000 C CNN
	1    5200 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2100 3550 2100
Wire Wire Line
	5000 2100 5200 2100
Wire Wire Line
	5000 2000 5250 2000
Wire Wire Line
	3400 2700 3400 2600
Wire Wire Line
	3400 2600 3550 2600
Wire Wire Line
	3350 1650 3450 1650
Wire Wire Line
	3450 1650 3450 1900
Wire Wire Line
	3450 1900 3550 1900
Wire Wire Line
	5000 1900 5100 1900
Wire Wire Line
	5100 1900 5100 1650
Wire Wire Line
	5100 1650 5200 1650
Wire Wire Line
	5100 2100 5100 2300
Wire Wire Line
	5100 2300 5250 2300
Connection ~ 5100 2100
Wire Wire Line
	3350 2000 3550 2000
Wire Wire Line
	3550 2200 3450 2200
Wire Wire Line
	3450 2200 3450 2100
Connection ~ 3450 2100
Wire Wire Line
	5000 2200 5100 2200
Connection ~ 5100 2200
Wire Wire Line
	5000 2600 5200 2600
Wire Wire Line
	5200 2600 5200 2700
Wire Wire Line
	5200 3000 5200 3100
Wire Wire Line
	3400 3000 3400 3100
Wire Wire Line
	3400 3100 5400 3100
Connection ~ 5200 3100
$EndSCHEMATC
